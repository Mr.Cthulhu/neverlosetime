# Henlloh hooman

A grandes rasgos lo que se hace es crear la ejecucion de una tarea como si esta fuese un daemon.

Mas info al respecto:
[cliclick](https://github.com/BlueM/cliclick/) ,
[launch](https://ss64.com/osx/launchctl.html) .

- 1. Instala desde brew launch.

``` bash
 brew install launch
```

- 2. Clona el repo a una ruta "segura"

``` bash
 git clone https://gitlab.com/Mr.Cthulhu/neverlosetime.git
```

- 3. Copia la tarea, su definicion y el binario a una ruta "segura"

``` bash
    cd neverlosetime && 
    mv never_lose_your_time.sh $HOME/.local && 
    mv com.haulmer.retail.neverlosetime.plist $HOME/.local &&
    mv cliclick $HOME/.local
```

- 3.5 Renombra en el archivo .plist, la frase TU_USUARIO por el nombre del mac.

- 4. Registra la definicion de la tarea en el gestor
*Quiza al copiar te solicite permisos de administrador (intenta anteponer el sudo)*

``` bash
cp $HOME/.local/com.haulmer.retail.neverlosetime.plist /Library/LaunchDaemons/ &&  launchctl load -w /Library/LaunchDaemons/com.haulmer.retail.neverlosetime.plist
```

- 5. Verifica que esta corriendo tu tarea

``` bash
 launchctl list | grep 'com.haulmer.retail.neverlosetime'
```

- 6. Para remover la tarea

``` bash
 launchctl unload -w /Library/LaunchDaemons/com.haulmer.retail.neverlosetime.plist && rm /Library/LaunchDaemons/com.haulmer.retail.neverlosetime.plist
```

- La primera vez te solicitara permiso de ejecucion desde el panel de seguridad y privacidad. Basta esperar el primer ciclo, dar los permisos solicitados y listo!.
